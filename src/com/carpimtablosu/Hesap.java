package com.carpimtablosu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Hesap extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hesap);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		Button btn1 = (Button)findViewById(R.id.btm1);
		Button btn2 = (Button)findViewById(R.id.btm2);
		Button btn3 = (Button)findViewById(R.id.btm3);
		btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			 Intent intent = new Intent();
			 intent.setClass(getApplicationContext(), Kolay.class);
			 startActivity(intent);
			}
		});
btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			 Intent intent = new Intent();
			 intent.setClass(getApplicationContext(), Orta.class);
			 startActivity(intent);
			}
		});
btn3.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	 Intent intent = new Intent();
	 intent.setClass(getApplicationContext(), Zor.class);
	 startActivity(intent);
	}
});

	}
	

}
