package com.carpimtablosu;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Kolay extends Activity {
	int sayi1,sayi2;
	int sonuc;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kolay);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final TextView txt1 = (TextView)findViewById(R.id.text1);
		final TextView txt2 = (TextView)findViewById(R.id.text2);
		final TextView text = (TextView)findViewById(R.id.yanlis);
		final EditText edt = (EditText)findViewById(R.id.edt2);
		Button cevap = (Button)findViewById(R.id.button1);
		Button yeni = (Button)findViewById(R.id.yenisoru);
		InputFilter[] FilterArray = new InputFilter[1]; 
		FilterArray[0] = new InputFilter.LengthFilter(2); 
		edt.setFilters(FilterArray);
		Random trt = new Random();
		sayi1 = trt.nextInt(10);
		sayi2 = trt.nextInt(10);
		sonuc=sayi1*sayi2;
		txt1.setText(String.valueOf(sayi1));
		txt2.setText(String.valueOf(sayi2));
		cevap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			int	gelen=Integer.parseInt(edt.getText().toString());
				if(gelen==sonuc)
				{
					text.setText(String.valueOf("Tebrikler Doğru"));
				}
				else {
					text.setText(String.valueOf("Tekrar Düşün"));
					}
				}
		});
		yeni.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edt.setText(String.valueOf(""));
				text.setText(String.valueOf(""));
				Random trt = new Random();
				sayi1 = trt.nextInt(10);
				sayi2 = trt.nextInt(10);
				sonuc=sayi1*sayi2;
				txt1.setText(String.valueOf(sayi1));
				txt2.setText(String.valueOf(sayi2));
				
			}
		});
		
	}
}
