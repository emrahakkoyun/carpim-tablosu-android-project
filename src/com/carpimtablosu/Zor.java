package com.carpimtablosu;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Zor extends Activity {
	int sayi1,sayi2;
	int sonuc;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zor);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final TextView txt1 = (TextView)findViewById(R.id.text1);
		final TextView text = (TextView)findViewById(R.id.yanlis);
		final EditText edt1 = (EditText)findViewById(R.id.edt1);
		final EditText edt2 = (EditText)findViewById(R.id.edt2);
		Button cevap = (Button)findViewById(R.id.button1);
		Button yeni = (Button)findViewById(R.id.yenisoru);
		InputFilter[] FilterArray = new InputFilter[1]; 
		FilterArray[0] = new InputFilter.LengthFilter(1); 
		edt1.setFilters(FilterArray);
		edt2.setFilters(FilterArray);
		Random trt = new Random();
		sayi1 = trt.nextInt(10);
		sayi2 = trt.nextInt(10);
		sonuc=sayi1*sayi2;
		txt1.setText(String.valueOf(sonuc));
		cevap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			int	gelen1=Integer.parseInt(edt1.getText().toString());
			int	gelen2=Integer.parseInt(edt2.getText().toString());
				if((gelen1*gelen2)==sonuc)
				{
					text.setText(String.valueOf("Tebrikler Doğru"));
				}
				else {
					text.setText(String.valueOf("Tekrar Düşün"));
					}
				}
		});
		yeni.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edt1.setText(String.valueOf(""));
				edt2.setText(String.valueOf(""));
				text.setText(String.valueOf(""));
				Random trt = new Random();
				sayi1 = trt.nextInt(10);
				sayi2 = trt.nextInt(10);
				sonuc=sayi1*sayi2;
				txt1.setText(String.valueOf(sonuc));
				
			}
		});
	}
}
