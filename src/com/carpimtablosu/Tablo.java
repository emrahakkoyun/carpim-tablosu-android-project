package com.carpimtablosu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Tablo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tablo);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		Button btn1=(Button)findViewById(R.id.btm1);
		Button btn2=(Button)findViewById(R.id.btm2);
		Button btn3=(Button)findViewById(R.id.btm3);
		Button btn4=(Button)findViewById(R.id.btm4);
		Button btn5=(Button)findViewById(R.id.btm5);
		Button btn6=(Button)findViewById(R.id.btm6);
		Button btn7=(Button)findViewById(R.id.btm7);
		Button btn8=(Button)findViewById(R.id.btm8);
		Button btn9=(Button)findViewById(R.id.btm9);
		btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
                intent.setClass(getApplicationContext(), Tablo1.class);
                startActivity(intent);
			}
		});
		btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			   Intent intent = new Intent();
			   intent.setClass(getApplicationContext(), Tablo2.class);
			   startActivity(intent);
			}
		});
btn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			   Intent intent = new Intent();
			   intent.setClass(getApplicationContext(), Tablo3.class);
			   startActivity(intent);
			}
		});
btn4.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo4.class);
	   startActivity(intent);
	}
});
btn5.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo5.class);
	   startActivity(intent);
	}
});
btn6.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo6.class);
	   startActivity(intent);
	}
});
btn7.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo7.class);
	   startActivity(intent);
	}
});
btn8.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo8.class);
	   startActivity(intent);
	}
});
btn9.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	   Intent intent = new Intent();
	   intent.setClass(getApplicationContext(), Tablo9.class);
	   startActivity(intent);
	}
});
	}
}
